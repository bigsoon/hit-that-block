import React from 'react';
import { Provider } from 'react-redux';
import { Text } from 'react-native';
import styled from 'styled-components';

import createStore from './store/createStore';

const store = createStore();

const StyledView = styled.View`
  flex: 1;
  background-color: #fff;
  align-items: center;
  justify-content: center;
`;

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <StyledView>
          <Text>Works</Text>
        </StyledView>
      </Provider>
    );
  }
}
