// @flow
import {
  createStore as createReduxStore,
  // applyMiddleware,
  compose
} from 'redux';
// import createSagaMiddleware from 'redux-saga';

import rootReducer from './combineReducers';
// import rootSaga from './sagasWatcher';

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
      })
    : compose;

// const sagaMiddleware = createSagaMiddleware();

const enhancer = composeEnhancers();
// applyMiddleware(sagaMiddleware)
// other store enhancers if any

export default function createStore() {
  const store = createReduxStore(rootReducer, enhancer);
  // sagaMiddleware.run(rootSaga);
  return store;
}
