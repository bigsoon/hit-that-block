// @flow
import { fromJS } from 'immutable';
import { combineReducers } from 'redux-immutable';

const initialState = fromJS({
  test: false
});

function testReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default combineReducers({ test: testReducer });
